##500Px photo retriever  [ ![Codeship Status for kevinyrock/python-500px](https://www.codeship.io/projects/acf8b090-f40a-0131-3cd8-568540174dde/status)](https://www.codeship.io/projects/28025)
 
###Usage
This is intended to be used on a RaspberryPi with external display as a photo frame.  Periodically, about once weekly, new photos will be pulled from 500px to keep the content fresh.
