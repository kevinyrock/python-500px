import fhp.api.five_hundred_px as f
import fhp.helpers.authentication as authentication

import urllib

from pprint import pprint
from fhp.models.photo import Photo

###########################
# Local Variables
###########################

i = 0 # loop counter: used to name files
PHOTOS_NEEDED = 20

###########################
# Authentication with 500px
###########################

key = authentication.get_consumer_key()
secret = authentication.get_consumer_secret()

client = f.FiveHundredPx(key, secret)


###########################
# Pull photos
# feature - popular / editors / upcoming / fresh_today
# image_size - 1, 2, 3, 4
# rpp - photos to return
###########################
results = client.get_photos(feature = 'editors', image_size = 4, rpp = PHOTOS_NEEDED)


###########################
# Loops through results
###########################
for photos in results:
    url = photos['image_url']
    
    urllib.urlretrieve(url, "photos/image%s.jpg" % i)
    
    i += 1
    if i == PHOTOS_NEEDED:
        break     
